
FROM centos:7
MAINTAINER ziyun wlcloudy@163.com

RUN yum -y update
RUN yum -y install openssh-server
RUN sed -i 's/UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config
ADD ssh_host_rsa_key /etc/ssh/ssh_host_rsa_key
ADD ssh_host_rsa_key.pub /etc/ssh/ssh_host_rsa_key.pub
RUN mkdir -p /root/.ssh
ADD authorized_keys /root/.ssh/authorized_keys

ADD run.sh /usr/local/sbin/run.sh
ADD apache-maven-3.3.9 /opt/apache-maven-3.3.9
# ADD jdk1.8.0_161 /opt/jdk1.8.0_161
# RUN chmod 755 /opt/jdk-8u161-linux-x64.tar.gz
# RUN chmod 755 /opt/apache-maven-3.3.9-bin.tar.gz
RUN chmod 755 /usr/local/sbin/run.sh
RUN echo "root:123456"|chpasswd


RUN yum install wget -y
RUN wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u161-b12/2f38c3b165be4555a1fa6e98c45e0808/jdk-8u161-linux-x64.tar.gz
# RUN wget http://apache.fayea.com/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
RUN cd /opt
RUN tar -xvf jdk-8u161-linux-x64.tar.gz -C /opt/
# RUN tar -xvf apache-maven-3.3.9-bin.tar.gz  -C /opt/
ENV JAVA_HOME /opt/jdk1.8.0_161
ENV MAVEN_HOME /opt/apache-maven-3.3.9
ENV CLASSPATH .:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV PATH $JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH
RUN yum install git -y
# expose memcached port
EXPOSE 22 
ENTRYPOINT ["/usr/local/sbin/run.sh"] 
# CMD ["-D"]
# RUN ["/bin/bash","-c","source /etc/profile"]
CMD ["/bin/bash"]
