使用Dockerfile制作基于Centos7支持ssh的java环境镜像

docker centos7 java8 ssh


在一些特定的环境中，我们经常重复的去配置系统环境，因此学习了下Dockerfile制作镜像, 
从而达到快速部署的目的。

1.环境准备 
   我的环境：Ubuntu16.04 
   docker版本：Docker version 17.12.0-ce（docker安装参考网:https://docs.docker.com/install/） 
2.在/root目录下新建docker_file目录用于存放Dockerfile和其他相关文件

$ mkdir docker_file
$ cd docker_file
3.编辑在容器中启动sshd服务及环境变量配置启用脚本 
   因为ssh连接容器，容器并没有启动/etc/profile ，因此需要在容器启动的时候export环境变量 
   export /root/.bashrc 的目的和上面一样，原因是docker 容器默认使用bashrc

$ vim run.sh
#!/bin/bash
export env
echo JAVA_HOME=$JAVA_HOME >> /etc/profile
echo JAVA_HOME=$JAVA_HOME >> /root/.bashrc
echo MAVEN_HOME=$MAVEN_HOME >> /etc/profile
echo MAVEN_HOME=$MAVEN_HOME >> /root/.bashrc
echo PATH=$PATH >> /etc/profile
echo PATH=$PATH >> /root/.bashrc
echo CLASSPATH=$CLASSPATH >> /etc/profile
echo CLASSPATH=$CLASSPATH >> /root/.bashrc
echo "export PATH JAVA_HOME CLASSPATH MAVEN_HOME" >> /etc/profile
echo "export PATH JAVA_HOME CLASSPATH MAVEN_HOME" >> /root/.bashrc
source /etc/profile
source /root/.bashrc
/usr/sbin/sshd -D
4.在宿主机上生成RSA密钥

$ ssh-keygen -t rsa -b 2048 -f /etc/ssh/ssh_host_rsa_key
$ cp /etc/ssh/ssh_host_rsa_key /etc/ssh/ssh_host_rsa_key.pub /root/sshd_centos cat ssh_host_rsa_key.pub>authorized_keys
5.在/root/docker_file目录下新建Dockerfile文件

   Dockerfile的常用语法，其中个人建议了解下ENTRYPOINT以及ENTRYPOINT 与CMD的区别 ，作者当时就是 
   在ssh 连接容器java环境未生效的问题上坑过，然后用ENTRYPOINT的方式解决，故此一提。

$ vim Dockerfile

FROM centos:7
MAINTAINER ziyun wlcloudy@163.com

RUN yum -y update
RUN yum -y install openssh-server
RUN sed -i 's/UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config
ADD ssh_host_rsa_key /etc/ssh/ssh_host_rsa_key
ADD ssh_host_rsa_key.pub /etc/ssh/ssh_host_rsa_key.pub
RUN mkdir -p /root/.ssh
ADD authorized_keys /root/.ssh/authorized_keys

ADD run.sh /usr/local/sbin/run.sh
RUN chmod 755 /usr/local/sbin/run.sh
RUN echo "root:123456"|chpasswd

RUN yum install wget -y
RUN wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u161-b12/2f38c3b165be4555a1fa6e98c45e0808/jdk-8u161-linux-x64.tar.gz
RUN wget http://apache.fayea.com/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
RUN cd /opt
RUN tar -xvf jdk-8u161-linux-x64.tar.gz -C /opt/
RUN tar -xvf apache-maven-3.3.9-bin.tar.gz  -C /opt/
ENV JAVA_HOME /opt/jdk1.8.0_161
ENV MAVEN_HOME /opt/apache-maven-3.3.9
ENV CLASSPATH .:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
ENV PATH $JAVA_HOME/bin:$MAVEN_HOME/bin:$PATH
RUN yum install git -y
EXPOSE 22 
ENTRYPOINT ["/usr/local/sbin/run.sh"] 
CMD ["/bin/bash"]

6.制作镜像

   镜像名称：slave-java8，镜像tag：1.0(默认 latest)

$ docker build -t slave-java8:1.0 .

执行效果：
    ....
    Successfully built 镜像ID
#将宿主机的10022端口映射到容器的22端口，并启动sshd服务。 
$ docker run -d -p 5555:22 slave-java8:1.0 
#进入容器（建议不使用attach命令进入容器）
$ docker exec -it 容器ID /bin/bash 
